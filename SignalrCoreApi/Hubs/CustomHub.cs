﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalrCoreApi.Hubs
{
    public class CustomHub:Hub
    {
        public async Task SendMessage()
        {
            await Clients.All.SendAsync("Test message", "payload");
        }
    }
}
