﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalrCoreApi.DataObjects
{
    public class Patient
    {
        public String Name { get; set; }
        public String MedicareNumber { get; set; }

        public int TicketNumber { get; set; } //used in waiting room only

        public String RequestedDoctor { get; set; }
        /// <summary>
        /// Used by FCM
        /// </summary>
        public String RegistrationToken { get; set; }
    }
}
