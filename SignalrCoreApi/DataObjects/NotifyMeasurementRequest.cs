﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalrCoreApi.DataObjects
{
    public class NotifyMeasurementRequest
    {
        public double Value { get; set; }
        public String Unit { get; set; }
        public String Type { get; set; }
    }

}
