﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalrCoreApi.DataObjects
{
    public class WaitingRoomAction
    {
        public String Type { get; set; }
        public Patient Patient { get; set; }
    }

    public class ActionType 
    { 
        public const String ADD = "ADD";
        public const String REMOVE = "REMOVE";
    }

}
