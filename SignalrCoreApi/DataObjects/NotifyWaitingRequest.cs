﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalrCoreApi.DataObjects
{
    public class NotifyWaitingRequest
    {
        public String PatientName { get; set; }
        public String MedicareNumber { get; set; }
        public String RequestedDoctor { get; set; }

        public String MedicareNumberRaw { get; set; }
        public String RegistrationToken { get; set; }
    }
}
