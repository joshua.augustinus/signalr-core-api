﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalrCoreApi.DataObjects
{
    public class LeaveWaitingRequest
    {
        public int TicketNumber { get; set; }
    }
}
