﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalrCoreApi.DataObjects
{
    public class BloodPressureRecord
    {
        public ValueUnit Systolic { get; set; }
        public ValueUnit Diastolic { get; set; }
        public ValueUnit PulseRate { get; set; }
    }
}
