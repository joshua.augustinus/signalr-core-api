﻿using SignalrCoreApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalrCoreApi.DataObjects
{
    public class NotifyWaitingPayload
    {
        public WaitingRoomAction Action {get;set;}
        public WaitingRoom WaitingRoom { get; set; } 
        public int QueuePosition { get; set; }
        
    }
}
