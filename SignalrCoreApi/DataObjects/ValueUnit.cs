﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalrCoreApi.DataObjects
{
    public class ValueUnit
    {
        public double Value { get; set; }
        public String Unit { get; set; }
    }
}
