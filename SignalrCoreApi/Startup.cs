using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiCommon;
using ApiCommon.Filters;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using SignalrCoreApi.Hubs;
using SignalrCoreApi.Models;

namespace SignalrCoreApi
{
    public class Startup
    {
        private const String CORS_POLICY_NAME = "MyPolicy";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var allowedOrigins = Configuration.GetSection("AllowedOrigins").GetChildren().Select(x => x.Value).ToArray();
            //For some reason this won't work if I allow all origins
            services.AddCors(options =>
            {
                options.AddPolicy(name: CORS_POLICY_NAME,
                                  builder =>
                                  {
                                      builder.WithOrigins(allowedOrigins).AllowAnyMethod().AllowAnyHeader().AllowCredentials();
                                  });
            });

            services.AddRazorPages();
            services.AddSignalR();


            services.AddControllers();
            services.AddSingleton< Decrypter>();
            services.AddSingleton<IPublicKeyProvider, PublicKeyProvider>();
            services.AddSingleton<WaitingRoom>();
            services.AddAuthentication()
               .AddScheme<BasicAuthenticationOptions, BasicAuthenticationHandler>("Basic", null);
            services.AddAuthentication().AddScheme<BearerAuthenticationOptions, BearerAuthenticationHandler>("Bearer", null);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env )
        {

            app.UseCors(CORS_POLICY_NAME);


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapRazorPages();
                endpoints.MapHub<CustomHub>("/hub");
            });

            //Get the discovery endpoints and them parse them to get public keys
            var discoveryEndpoints = Configuration.GetSection("DiscoveryEndpoints").GetChildren().Select(x => x.Value).ToArray();
            var publicKeyProvider = app.ApplicationServices.GetRequiredService<IPublicKeyProvider>();
            publicKeyProvider.ParseDiscoveryEndpoints(discoveryEndpoints);
        }


    }
}
