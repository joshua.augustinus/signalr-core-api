﻿using SignalrCoreApi.DataObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace SignalrCoreApi.Models
{
    public class WaitingRoom
    {

        public List<Patient> Patients { get; set; }
        private int LastTicketNumber { get; set; }


        public WaitingRoom()
        {
            Patients = new List<Patient>();
            LastTicketNumber = 0;
        }

        public int GetLastTicketNumber()
        {
            return LastTicketNumber;
        }

        /// <summary>
        /// Returns -1 if not in queue. First in line means 1.
        /// </summary>
        /// <param name="ticketNumber"></param>
        /// <returns></returns>
        public int GetQueuePosition(int ticketNumber)
        {
            var result = Patients.FindIndex(x => x.TicketNumber == ticketNumber);
            return result + 1;
        }


        /// <summary>
        /// Adds/Removes to Waiting Room. If adding, will also give ticket number to Patient object
        /// </summary>
        /// <param name="action"></param>
        public void ExecuteAction(WaitingRoomAction action)
        {
            if (action.Type == ActionType.ADD)
            {

                if (!Patients.Any(x => x.MedicareNumber == action.Patient.MedicareNumber))
                {
                    LastTicketNumber++;
                    action.Patient.TicketNumber = LastTicketNumber;
                    Patients.Add(action.Patient);
                  
                }
            }
            else if (action.Type == ActionType.REMOVE)
            {
                if(!String.IsNullOrEmpty(action.Patient.MedicareNumber))
                    Patients.RemoveAll(x => x.MedicareNumber == action.Patient.MedicareNumber);
                else
                    Patients.RemoveAll(x => x.Name == action.Patient.Name);
            }
        }

        public Patient FindPatient(String medicareNumber)
        {
            if (String.IsNullOrEmpty(medicareNumber)){
                return null;
            }
            return Patients.FirstOrDefault(x => x.MedicareNumber == medicareNumber);
        }



    }
}
