﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using SignalrCoreApi.DataObjects;
using SignalrCoreApi.Helpers;
using SignalrCoreApi.Hubs;
using SignalrCoreApi.Models;

namespace SignalrCoreApi.Controllers
{
    [ApiController]
    public class WaitingController : ControllerBase
    {
        private WaitingRoom _waitingRoom;
        IHubContext<CustomHub> _customHub;
        public WaitingController(WaitingRoom room, IHubContext<CustomHub> hub)
        {
            _waitingRoom = room;
            _customHub = hub;
        }

        /// <summary>
        /// Add self to waiting room
        /// </summary>
        /// <param name="request"></param>
        /// <param name="useJwtSubject">The person getting added will be the jwt subject</param>
        /// <returns></returns>
        [HttpPost]
        [Route("waiting")]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<object> NotifyWaiting(NotifyWaitingRequest request, bool useJwtSubject = false)
        {
            if (useJwtSubject)
            {
                request.PatientName = User.Identity.Name;
            }
          

            var medicareNumber = request.MedicareNumber;
            //Give the user a medicare card number if he doesnt' have one
            if(String.IsNullOrEmpty(request.MedicareNumber))
            {
                if (String.IsNullOrEmpty(request.MedicareNumberRaw))
                    medicareNumber = MedicareHelper.GetMedicareNumber();
                else
                {
                    medicareNumber = MedicareHelper.ParseMedicareCardNumberRaw(request.MedicareNumberRaw);
                    //Now we should fetch the name from MyHealthRecord API but for this demo project we won't

                    if (medicareNumber == EnvironmentVariableHelper.GetVariable(EnvironmentKeys.PERSONAL_MEDICARE_NUMBER))
                        request.PatientName = EnvironmentVariableHelper.GetVariable(EnvironmentKeys.PERSONAL_MEDICARE_NAME);
                    else
                        request.PatientName = "Unknown patient";
                }
                 
            }

            if (String.IsNullOrEmpty(request.PatientName))
                return BadRequest("patientName cannot be blank");

            //Find patient if already exists in waiting room
            Patient patient = _waitingRoom.FindPatient(medicareNumber);
            var patientAlreadyInRoom = patient != null;
            if (!patientAlreadyInRoom)
            {
                //Otherwise create new patient object
                patient = new Patient { MedicareNumber = medicareNumber, Name = request.PatientName, RequestedDoctor = request.RequestedDoctor, RegistrationToken = request.RegistrationToken };
            }
       

            var action = new WaitingRoomAction { Type = ActionType.ADD, Patient = patient };
            _waitingRoom.ExecuteAction(action);
            var qPosition = _waitingRoom.GetQueuePosition(patient.TicketNumber);
            var payload = new NotifyWaitingPayload { Action = action, WaitingRoom = _waitingRoom, QueuePosition = qPosition  };
            if(!patientAlreadyInRoom)
                await _customHub.Clients.All.SendAsync("waiting", payload);
            return Ok(payload);
        }




        [HttpDelete]
        [Route("waiting")]
        public async Task<object> NotifyWaitingDelete(LeaveWaitingRequest request)
        {
            if (request.TicketNumber <=0 )
                return BadRequest("TicketNumber cannot be blank");

            var patient = _waitingRoom.Patients.FirstOrDefault(x => x.TicketNumber == request.TicketNumber);
            NotifyWaitingPayload payload;

            if (patient != null)
            {
                var action = new WaitingRoomAction { Type = ActionType.REMOVE, Patient = patient };
                _waitingRoom.ExecuteAction(action);
                payload = new NotifyWaitingPayload { Action = action, WaitingRoom = _waitingRoom };
                await _customHub.Clients.All.SendAsync("waiting", payload);
                return Ok(payload);
            }

            payload = new NotifyWaitingPayload { Action = null, WaitingRoom = _waitingRoom };
            return Ok(payload);
          
        
        }

        [HttpGet]
        [Route("waiting")]
        public object GetWaitingRoom()
        {           
            return Ok(_waitingRoom);
        }
    }
}
