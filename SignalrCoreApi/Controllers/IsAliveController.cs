﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiCommon;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SignalrCoreApi.Helpers;

namespace SignalrCoreApi.Controllers
{
    public class IsAliveController : ControllerBase
    {
        private Decrypter _decrypter;
        private IConfiguration _configuration;
        public IsAliveController( Decrypter decrypter, IConfiguration configuration )
        {
            _decrypter = decrypter;
            _configuration = configuration;
        }
        [HttpPost]
        [HttpGet]
        [Route("isalive")]
        public object Get()
        {
            var allowedOrigins = _configuration.GetSection("AllowedOrigins").GetChildren().Select(x => x.Value).ToArray();
            var personalMedicareNumber = EnvironmentVariableHelper.GetVariable(EnvironmentKeys.PERSONAL_MEDICARE_NUMBER);
            if (personalMedicareNumber == null)
                throw new Exception("PERSONAL_MEDICARE_NUMBER should not be null");

            var personalMedicareName = EnvironmentVariableHelper.GetVariable(EnvironmentKeys.PERSONAL_MEDICARE_NAME);
            if (personalMedicareName == null)
                throw new Exception("PERSONAL_MEDICARE_NAME should not be null");

            return new { allowedOrigins = allowedOrigins};
        }
    }
}
