﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using SignalrCoreApi.DataObjects;
using SignalrCoreApi.Hubs;
using SignalrCoreApi.Models;

namespace SignalrCoreApi.Controllers
{

    /// <summary>
    /// This controller notifies the hub clients
    /// </summary>
    [ApiController]
    [Authorize(AuthenticationSchemes ="Bearer")]
    public class NotifyController : ControllerBase
    {
        IHubContext<CustomHub> _customHub;
        WaitingRoom _waitingRoom;

        public NotifyController(IHubContext<CustomHub> context, WaitingRoom waitingRoom)
        {
            _customHub = context; //Dependency injection
            _waitingRoom = waitingRoom;
        }

        [HttpPost]
        [Route("notify/measurement")]
        public async Task NotifyMeasurement(NotifyMeasurementRequest payload)
        {
           await _customHub.Clients.All.SendAsync("measurement", payload);
        }

        [HttpPost]
        [Route("notify/bloodpressure")]
        public async Task NotifyBpMeasurement(BloodPressureRecord payload)
        {
            await _customHub.Clients.All.SendAsync("bloodPressure", payload);
        }


        /// <summary>
        /// Returns the queue position of the patient. The patient is the sub in the JWT token
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("notify/ignorecall")]
        [Obsolete("Use ticket/{ticketNumber}/ignorecall instead")]
        public async Task<object> NotifyIgnoreCall()
        {
            var username = User.Identity.Name;
            var patient = _waitingRoom.Patients.FirstOrDefault(x => x.Name == username);
            var qPos = -1;
            if (patient != null)
            {
                await _customHub.Clients.All.SendAsync("ignoreCall", patient);
                qPos = _waitingRoom.GetQueuePosition(patient.TicketNumber);
                return new  { patient=patient, queuePosition= qPos };
            }
            else
            {
                return new { patient = patient, queuePosition = qPos };
            }
             
        }

    }
}
