﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using SignalrCoreApi.Hubs;
using SignalrCoreApi.Models;

namespace SignalrCoreApi.Controllers
{

    [ApiController]
    public class TicketController : ControllerBase
    {
        private WaitingRoom _waitingRoom;
        private IHubContext<CustomHub> _customHub;
        public TicketController(IHubContext<CustomHub> context, WaitingRoom waitingRoom)
        {
            _waitingRoom = waitingRoom;
            _customHub = context;
        }
        [HttpGet]
        [Route("ticketnumber/{ticketNumber}/queueposition")]
        public object GetQueuePosition(int ticketNumber)
        {
            return _waitingRoom.GetQueuePosition(ticketNumber);
        }

        [HttpPost]
        [Route("ticketnumber/{ticketNumber}/ignorecall")]
        public async Task<object> IgnoreCall(int ticketNumber)
        {
            var patient = _waitingRoom.Patients.FirstOrDefault(x => x.TicketNumber==ticketNumber);
            var qPos = -1;
            if (patient != null)
            {
                await _customHub.Clients.All.SendAsync("ignoreCall", patient);
                qPos = _waitingRoom.GetQueuePosition(patient.TicketNumber);
                return new { patient = patient, queuePosition = qPos };
            }
            else
            {
                return new { patient = patient, queuePosition = qPos };
            }
        }

        [HttpPost]
        [Route("ticketnumber/{ticketNumber}/endcall")]
        public async Task EndCall(int ticketNumber)
        {
            await _customHub.Clients.All.SendAsync("endCall", new { ticketNumber= ticketNumber});
        }
    }
}
