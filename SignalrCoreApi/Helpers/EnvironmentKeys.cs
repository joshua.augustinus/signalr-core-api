﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalrCoreApi.Helpers
{
    /**
     * Keys for environment variables
     */
    public class EnvironmentKeys
    {
        public const String PERSONAL_MEDICARE_NUMBER = "PERSONAL_MEDICARE_NUMBER";
        public const String PERSONAL_MEDICARE_NAME = "PERSONAL_MEDICARE_NAME";
    }
}
