﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalrCoreApi.Helpers
{
    public static class MedicareHelper
    {
        private static List<long> MedicareCardNumbers = new List<long>();

        private static readonly Random _random = new Random();
        public static String GetMedicareNumber()
        {
            //rand number
            long number = 0;
            var foundNumber = false;
            while (!foundNumber)
            {
                number = LongRandom(1000000000, 9999999999);
                if (!MedicareCardNumbers.Any(x => x  == number ))
                {
                    foundNumber = true;
                }
            }

            MedicareCardNumbers.Add(number);
            return number.ToString();
        }

        public static String ParseMedicareCardNumberRaw(String rawNumber)
        {
            //Raw will be of the form:
            //;610072=267035830=3=280223?
            var split = rawNumber.Split("=");
            return split[1].ToString() + split[2].ToString();

        }


        private static long LongRandom(long min, long max)
        {
            long result = _random.Next((Int32)(min >> 32), (Int32)(max >> 32));
            result = (result << 32);
            result = result | (long)_random.Next((Int32)min, (Int32)max);
            return result;
        }
    }
}
