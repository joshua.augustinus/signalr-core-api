﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalrCoreApi.Helpers
{
    public class EnvironmentVariableHelper
    {
        public static String GetVariable(String key)
        {
            var result = Environment.GetEnvironmentVariable(key, EnvironmentVariableTarget.User);
            if (result == null)
                result = Environment.GetEnvironmentVariable(key, EnvironmentVariableTarget.Machine);
            if (result == null)
                result = Environment.GetEnvironmentVariable(key, EnvironmentVariableTarget.Process);
            return result;
        }
    }
}
