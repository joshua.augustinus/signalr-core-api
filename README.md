# Setup
This Api uses the ApiCommon package which source code is located in https://gitlab.com/joshua.augustinus/user-api

The actual nuget package is located in [another repo](https://gitlab.com/joshua.augustinus/common-nuget-packages).
You can add it via using a nuget package.

1. Clone the [nuget package repo](https://gitlab.com/joshua.augustinus/common-nuget-packages)
```
git clone https://gitlab.com/joshua.augustinus/common-nuget-packages.git
```

2. Open Visual Studio on the SignalrCoreApi.sln

3. Go to Tools > Nuget package manager > Package manager settings > Package sources

4. Add a package source. Name should be "Local" and the source should be the folder where you downloaded the common-nuget-packages.

5. You should now be able to build the solution and it will automatically use the packages in your Local packge source.